/**
 * Copyright 2018
 */
package com.staxter.controller;

import com.staxter.model.RegisterUser;
import com.staxter.model.User;
import com.staxter.model.UserCreatedResponse;
import com.staxter.service.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/userservice")
@AllArgsConstructor
@Log
public class RegistrationController {

    public static final String ACCEPT_APPLICATION_JSON = "Accept=application/json";

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/register", method = RequestMethod.POST, headers = ACCEPT_APPLICATION_JSON, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity registerUser(@RequestBody RegisterUser registerUser) {

        final User registeredUser = userService.createUser(registerUser);
        UserCreatedResponse response = new UserCreatedResponse(
                registeredUser.getId(),
                registeredUser.getFirstName(),
                registeredUser.getLastName(),
                registeredUser.getUserName());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
