/**
 * Copyright 2018
 */
package com.staxter.service;

import com.staxter.error.UserAlreadyExistException;
import com.staxter.model.RegisterUser;
import com.staxter.model.User;

public interface UserService {

    User findUserByUserName(String userName);

    User createUser(RegisterUser user) throws UserAlreadyExistException;
}
