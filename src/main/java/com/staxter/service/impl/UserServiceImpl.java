/**
 * Copyright 2018
 */
package com.staxter.service.impl;

import com.staxter.error.UserAlreadyExistException;
import com.staxter.model.RegisterUser;
import com.staxter.model.User;
import com.staxter.model.UserRole;
import com.staxter.repository.UserRepository;
import com.staxter.service.UserService;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@Log
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private UserService userService;

    @Override
    public User findUserByUserName(String userName) {
        return userRepository.findByUserName(userName);
    }

    @Override
    public User createUser(RegisterUser newUser) throws UserAlreadyExistException {
        if (userExist(newUser.getUserName())) {
            log.info("User with this username already exist. username: " + newUser.getUserName());
            throw new UserAlreadyExistException();
        }
        final User user = new User();
        user.setFirstName(newUser.getFirstName());
        user.setLastName(newUser.getLastName());
        user.setUserName(newUser.getUserName());
        user.setPlainTextPassword(newUser.getPassword());
        user.setHashedPassword(passwordEncoder.encode(newUser.getPassword()));
        UserRole userRole = new UserRole();
        userRole.setId(1);
        userRole.setName("regularCustomer");
        user.setRole(userRole);
        return userRepository.save(user);
    }

    private boolean userExist(final String userName) {
        return userService.findUserByUserName(userName) != null;
    }
}
