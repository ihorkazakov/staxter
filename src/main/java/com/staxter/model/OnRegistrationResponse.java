/**
 * Copyright 2018
 */
package com.staxter.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OnRegistrationResponse {

    private String code;
    private String description;

    public OnRegistrationResponse(String code, String description) {
        this.code = code;
        this.description = description;
    }
}
