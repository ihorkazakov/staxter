/**
 * Copyright 2018
 */
package com.staxter.model;

import com.staxter.util.EntityHelper;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.SQLDelete;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "USERS")
@SQLDelete(sql = "update USER SET SIGN = ID WHERE ID = ? AND VERSION = ?")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class User implements UserDetails {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "ID", updatable = false, nullable = false)
    private String id;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "USER_NAME")
    private String userName;

    @Column(name = "PASSWORD")
    private String plainTextPassword;

    @Column(name = "HASHED_PASSWORD")
    private String hashedPassword;

    @ManyToOne(cascade = CascadeType.ALL)
    @Fetch(FetchMode.SELECT)
    @JoinColumn(name = "ID_USER_ROLE_FK")
    private UserRole role;

    @Column(name = "SIGN", nullable = false, precision = 28)
    private BigDecimal sign;

    @Column(name = "UPD_DATE", nullable = false)
    private Date updDate;

    /**
     * method called before create entity
     * see {@link PrePersist}
     */
    @PrePersist
    public void prePersist() {
        sign = EntityHelper.ACTUAL_SIGN;
        setUpdDate(new Date());
    }

    /**
     * method called before update and delete entity
     * see {@link PreUpdate}, {@link PreRemove}
     */
    @PreUpdate
    @PreRemove
    public void preChanged() {
        setUpdDate(new Date());
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        for (UserRolesEnum role : UserRolesEnum.values()) {
            authorities.add(new SimpleGrantedAuthority(role.name()));
        }
        return authorities;
    }

    public String getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    @Override
    public String getPassword() {
        return plainTextPassword;
    }

    @Override
    public String getUsername() {
        return userName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(firstName, user.firstName) &&
                Objects.equals(lastName, user.lastName) &&
                Objects.equals(userName, user.userName) &&
                Objects.equals(role, user.role);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, userName, role);
    }
}
