/**
 * Copyright 2018
 */
package com.staxter.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RegisterUser {

    private String firstName;
    private String lastName;
    private String userName;
    private String password;
}
