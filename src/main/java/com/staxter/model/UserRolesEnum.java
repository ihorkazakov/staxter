/**
 * Copyright 2018
 */
package com.staxter.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum UserRolesEnum {

    USER(1),
    ADMIN(2);

    @Getter
    private final int intValue;
}
