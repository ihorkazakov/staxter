/**
 * Copyright 2018
 */
package com.staxter.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class UserCreatedResponse {

    private String id;
    private String firstName;
    private String lastName;
    private String userName;
}
