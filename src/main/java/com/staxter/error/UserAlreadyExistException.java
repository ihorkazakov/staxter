/**
 * Copyright 2018
 */
package com.staxter.error;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public final class UserAlreadyExistException extends RuntimeException {

    private String code;
    private String description;
}
