/**
 * Copyright 2018
 */
package com.staxter.error;

import com.staxter.model.OnRegistrationResponse;
import com.staxter.util.EntityHelper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    public RestResponseEntityExceptionHandler() {
        super();
    }

    @ExceptionHandler({UserAlreadyExistException.class})
    @ResponseBody
    @ResponseStatus(HttpStatus.CONFLICT)
    public OnRegistrationResponse handleUserAlreadyExist(final UserAlreadyExistException ex) {
        logger.error("409 Status Code", ex);
        OnRegistrationResponse response = new OnRegistrationResponse(
                EntityHelper.USER_ALREADY_EXIST,
                EntityHelper.USER_ALREADY_EXIST_DESC);
        return response;
    }
}
