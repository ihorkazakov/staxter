/**
 * Copyright 2018
 */
package com.staxter.util;

import lombok.NoArgsConstructor;

import java.math.BigDecimal;

public class EntityHelper {

    public static final BigDecimal ACTUAL_SIGN = BigDecimal.ZERO;
    public static final String USER_ALREADY_EXIST = "USER_ALREADY_EXISTS";
    public static final String USER_ALREADY_EXIST_DESC = "A user with the given username already exists";
}
